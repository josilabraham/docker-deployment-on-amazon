# Docker Lab Exercise

## Creating an Image

1. Complete steps listed in [Quick Start](Quick_Start.md)
2. Install sample flask app via GIT

```bash
git clone https://github.com/prakhar1989/docker-curriculum

cd docker-curriculum/flask-app
```

3. Repo already contains a Dockerfile, so creating one won't be necessary
4. Run the following command to build the image:

```bash
#Replace <username> and <image name> with respective values
docker build -t <username>/<image name>
```

5. Run image to see if it works:

```bash
#Replace <username> and <image name> with respective values
docker run -p 8888:5000 prakhar1989/catnip
```
## Image Deployment on Amazon Elastic Beanstalk

1. Login to AWS and go to the Elastic Beanstalk page; click “Create New Application”
2. Give your application a name and description
3. Create a new environment and choose the Web Server option
4. On next screen, choose Docker as the pre-configured platform
5. Upload Dockerrun.aws.json file located in app folder and launch with default configurations

If you're uploading your own application and don't have a Dockerrun.aws.json file, you must make your own according to documentation specifications: [Dockerrun.aws.json](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_docker_image.html#create_deploy_docker_image_dockerrun)

Use the URL at the top of the page to launch the app!
