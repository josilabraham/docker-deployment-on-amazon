# Getting Started With Docker
Here are some steps to setup Docker for the first time

## Requirements
Create accounts for the following services:
- [AWS](http://aws.amazon.com/)
- [Docker Hub](https://hub.docker.com/)

Next, make sure to install the following on your computer:

- [Git](https://git-scm.com)
- [Docker CE](https://www.docker.com/community-edition)
OR
- [Docker Toolbox](https://docs.docker.com/toolbox/toolbox_install_windows/)