# Docker Deployment on Amazon Web Services


## Files

- [Quick Start Guide](Quick_Start.md)
- [Docker Lab Excercise](Docker_Lab.md)
- [Docker Deployment Presentation](https://docs.google.com/presentation/d/1F3wGKFoJDwVEGu-cV76VrMX55R6vukAgDJba4HRcoAg/edit?usp=sharing)

## Resources

- [Docker Documentation](https://docs.docker.com/)
- [Amazon ECS Documentation](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)
- [Amazon Elastic Beanstalk Documentation](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/Welcome.html)
- [Docker Tutorial](https://reactjs.org/tutorial/tutorial.html)
- [Dockerrun.aws.json Documentation](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_docker_image.html#create_deploy_docker_image_dockerrun)